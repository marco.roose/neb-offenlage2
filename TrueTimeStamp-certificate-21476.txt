-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

============================================================================
   Certificate
============================================================================
In conjunction with the file(s) that produce the following SHA-2 fingerprint,
and in conjunction with the verification procedures available on
TrueTimeStamp.org (copy available below), this certifies that the following
file existed and was time-stamped on:

Time: January 26, 2019 5:08:38 am GMT
Stored SHA-2 Fingerprint:
2fe626c0be39f1eeb894a5cc6721e12c411d94a1238627ec850a70805287585f
Certificate Type: multi-file
Constituent Files:
be5d68fb327a51ba4a627f8c97fffc213878386eaa66eab4cd59c7e65b30fd83	1_uebersicht_geltungsbereiche_henke__002_.pdf
ae94426a4622b0ede29871ca2f4ac86cd3ac6e5cdb15e20149e3793ff9966e26	3_bpl-nr-13_entwurf_festsetzungen__002_.pdf
25c0af835a584c8247cfba95dc073f23840f2e6e3ba68f3636163082306adc5f	4_bpl-nr-13_entwurf_begruendung_inkl._anhaenge__002_.pdf
8761e331a80e21b30dee48d7a3d14781fa9fee002a483bbb073a088cccdc5af6	neu_2_bpl-nr-13_entwurf_planzeichnung_fa__002__-_kopie.pdf
48dfb63da448c6b5afed40638372594bc6707c02b0e5c7b203a89d743606a986	screenshot bekanntmachungen.png
b85f9fffdb0f003eaf105e3e372c31917888e61ab8a678222370d51ec69411ea	umweltrelevante_stellungnahmen_04.pdf

============================================================================
   Certificate Information
============================================================================
Authority: True Time Stamp ( http://TrueTimeStamp.org )
Certificate Number: 21476
Sequential Validity Chain: 2d35d133f127f35858d75cf6c915118f42eddf46ee6bfc688f65a86bde482c7b

============================================================================
   Important Note
============================================================================
1 - Backup copy of the original unaltered file must be kept to authenticate
    this certificate. 
2 - Some editing programs may inadverently alter files by including the
    "save time" in the file contents, or changing character encoding, even if
     no edits are made.  Back-up using your operating system's copy function
     rather than "Save As".

============================================================================
   Verification Procedures
============================================================================
Online - Single File Certificate:
  - Supply the ORIGINAL FILE to http://TrueTimeStamp.org for verification.

Online - Multiple File Certificate:
  - Supply THIS CERTIFICATE to http://TrueTimeStamp.org for verification.
  - Additionally, for each file that you want to prove existed at the time
    point above, you must confirm that the SHA-2 of these file(s) matches
    those listed above (see instructions "Calculate SHA-2 Fingerprint of a
    file" below).

Offline Procedures:
  - Use these procedures if http://TrueTimeStamp.org ceases to exist, or if
    you would like to independently confirm the electronic signature of this
    certificate.
  - Obtain GPG software ( https://www.gnupg.org/download )    
  - Obtain the True Time Stamp Public Key, from any of the servers below, by
        searching by email:
        EMAIL: signing-department@TrueTimeStamp.org
        KEY ID: 0x6f3b2e6ab748a8f8
        KEY Fingerprint: 0x83289060f40ded088cf246b56f3b2e6ab748a8f8
        - http://truetimestamp.org/public-keys
        - https://pgp.mit.edu
        - http://keyserver.cns.vt.edu:11371
        - http://keyserver.lsuhscshreveport.edu:11371
        - http://keyserver.ubuntu.com
        - https://keyserver.pgp.com
        - http://keyserver.searchy.nl:11371
        - http://keyserver.compbiol.bio.tu-darmstadt.de:11371
  - Download the appropriate key, save as TrueTimeStamp-key4-DSA-3072.asc
  - Optionally, verify the fingerprint of the public key.
        PUBLIC KEY SHA-2 FINGERPRINT, base64 representation, UTF-8,
        UNIX-style line breaks, without headers or footers:
        16fecee8a5fd4cc39facfd1c5db36fe2eec553cf0dfa2e7496d4a3556027790e
  - Import the downloaded public-key via command-line:
       gpg --import TrueTimeStamp-key4-DSA-3072.asc
  - Verify the authenticity of this certificate via command-lines:
       gpg --import TrueTimeStamp-key4-DSA-3072.asc
       gpg --verify myCertificateFile
  - For multi-file certificates, you may also confirm that:
  		Stored SHA-2 Fingerprint matches the "Constituent Files" section
  		   - Copy & Paste text under "Constituent Files" section into a
                separate file, and save without trailing spaces and using
                UNIX-style newlines.
  		   - Calculate SHA-2 of this file, and confirm that this matches the
  		        Stored SHA-2 fingerprint.
  - For each file that you want to confirm the time stamp, calculate its SHA-2
       fingerprint, and confirm that this is present in this certificate above.

To Calculate SHA-2 Fingerprint of a file:
   - Online at http://TrueTimeStamp.org
   - Using software such as sha256sum, or openssl, with the command-lines:
        sha256sum MyFileName
        openssl dgst -sha256 MyFileName

Sequential Validity Chain:
   - Guards against back-dating any time stamp, or removing any time stamp
         in the future.
   - Consists of SHA-2( Sequential Validity Chain of previous certificate,
         SHA-2 of current file, UNIX Time Stamp).
   - Validity Chains are intermittently submitted to other Time Stamping
         Services.
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.14 (GNU/Linux)

iF4EAREIAAYFAlxL61YACgkQbzsuardIqPjo5wD+MYW0ZU1pO0LcNe1585d8MQ0z
3aTDAumDQBluVCZ6mgUA/0AvkYj43MwvXJS3sIy0vsw1bqhuaFYBwsufncEXsw9r
=d5yM
-----END PGP SIGNATURE-----
